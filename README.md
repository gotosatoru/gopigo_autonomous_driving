# README #

This is some code that makes the gopigo more or less autonomous in driving around. The settings can be quite specific (turning speed, left and right angle for the sensor etc) so finetune those using the constants in the code.

Todo;
 
* add sound using a USB speaker and make it tell things while driving
* adding a full turn in case the angles only show (too) short distances
* anything that is fun and I can not come up with now :-)


This code is free so make adjustments and show your gopigo stuff!

Video of working code: https://www.youtube.com/watch?v=jwL0e9VrbBo

Regards,
Rob alias BreinBaas

breinbaasnl@gmail.com