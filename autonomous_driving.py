import gopigo as go
import time
import random
import sys

MINDISTANCE = 50
ANGLEFORWARD = 70
ANGLERIGHT = 0
ANGLELEFT = 141
DT45TURN = 0.55

def full_turn():
    turn_left(180)

def turn_left(degrees):
    print "Turning left for approx %d degrees" % degrees
    go.left()
    time.sleep(-1*degrees / 45.0 * DT45TURN)

def turn_right(degrees):
    print "Turning right for approx %d degrees" % degrees
    go.right()
    time.sleep(degrees / 45.0 * DT45TURN)

def drive_forward():
    print "Driving forward."
    no_problem = True
    while no_problem:
        go.bwd()
        time.sleep(0.3)
        dist = go.us_dist(15)

        if dist < MINDISTANCE:
            print "Found something standing in the way at %d centimeters." % dist
            no_problem = False
        else:
            print "Seems safe for at least %d centimeters." % dist
    go.stop()

def turn_to_best_angle():
    go.enable_servo()
    max_dist = 0
    best_angle = -1
    for i in range(ANGLERIGHT, ANGLELEFT, 35):
        go.servo(i)
        time.sleep(0.3)
        dist = go.us_dist(15)
        print "Looking at angle %d gives a distance of %d cm." % (i, dist)
        if dist > max_dist:
            best_angle = i
            max_dist = dist
    
    if max_dist < MINDISTANCE:
        print "Ok, I think I am stuck here, every angle gives a distance less than the minimum of %d centimeters" % MINDISTANCE
        go.stop()
        sys.exit()
        #TODO: you could try a full turn

    print "Ok, concentrating on angle %d." % best_angle
    go.servo(ANGLEFORWARD)
    time.sleep(1.0)
    go.disable_servo()    

    dangle = best_angle - ANGLEFORWARD
    if dangle < 0:
        turn_left(dangle)
    elif dangle > 0:
        turn_right(dangle)

def drive_autonomous():
    while(1):
        turn_to_best_angle()
        drive_forward()

if __name__=="__main__":

    try:
        drive_autonomous()
    except:
        go.stop()
        go.disable_servo()
        raise
    
    


            
            
